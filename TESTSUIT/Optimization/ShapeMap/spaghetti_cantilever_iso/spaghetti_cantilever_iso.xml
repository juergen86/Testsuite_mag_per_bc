<?xml version="1.0" encoding="UTF-8"?>
<cfsSimulation xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns="http://www.cfs++.org/simulation">
  
  <documentation>
    <title> feature mapping using linear spline ("spaghetti") with p-norm for feature aggregation
    </title>
    <authors>
      <author>Jannis Greifenstein</author>
    </authors>
    <date>2022-06-01</date>
    <keywords>
      <keyword>optimization</keyword>
    </keywords>
    <references>
    </references>
    <isVerified>no</isVerified>
        <description> cantilever optimization isotropic material </description>
  </documentation>

  <fileFormats>
    <output>
      <hdf5/>
      <info/>
    </output>
    <materialData file="mat.xml" format="xml"/>
  </fileFormats>

  <domain geometryType="plane">
    <regionList>
      <region material="99lines" name="mech"/>
    </regionList>
    <nodeList>
      <nodes name="load">
        <coord x="1.0" y="0.0"/>
      </nodes>
      <nodes name="fix1">
        <coord x="0.0" y="1.0"/>
      </nodes>
      <nodes name="fix2">
        <coord x="0.0" y="0.0"/>
      </nodes>
    </nodeList>
  </domain>


  <sequenceStep index="1">
    <analysis>
      <static/>
    </analysis>

    <pdeList>
      <mechanic subType="planeStress">
        <regionList>
          <region name="mech"/>
        </regionList>

        <bcsAndLoads>
           <fix name="west"> 
              <comp dof="x"/> <comp dof="y"/> 
           </fix>
           <force name="load" >
             <comp dof="y" value="-.5"/>
           </force>
           
        </bcsAndLoads>
        <storeResults>
          <elemResult type="mechPseudoDensity">
            <allRegions/>
          </elemResult>
          <elemResult type="optResult_1">
            <allRegions/>
          </elemResult>
          <elemResult type="optResult_2">
            <allRegions/>
          </elemResult>
          <elemResult type="optResult_3">
            <allRegions/>
          </elemResult>
          <elemResult type="optResult_4">
            <allRegions/>
          </elemResult>
          <elemResult type="optResult_5">
            <allRegions/>
          </elemResult>
          <elemResult type="optResult_6">
            <allRegions/>
          </elemResult>
          <elemResult type="optResult_7">
            <allRegions/>
          </elemResult>
          <elemResult type="optResult_8">
            <allRegions/>
          </elemResult>
          <elemResult type="optResult_9">
            <allRegions/>
          </elemResult>
          <elemResult type="optResult_10">
            <allRegions/>
          </elemResult>
          <elemResult type="optResult_11">
            <allRegions/>
          </elemResult>
          <elemResult type="optResult_12">
            <allRegions/>
          </elemResult>
          <elemResult type="optResult_13">
            <allRegions/>
          </elemResult>
          <elemResult type="optResult_14">
            <allRegions/>
          </elemResult>
          <elemResult type="optResult_15">
            <allRegions/>
          </elemResult>
          <elemResult type="optResult_16">
            <allRegions/>
          </elemResult>
          <elemResult type="optResult_17">
            <allRegions/>
          </elemResult>
          <elemResult type="optResult_18">
            <allRegions/>
          </elemResult>
          <elemResult type="optResult_19">
            <allRegions/>
          </elemResult>
          <elemResult type="optResult_20">
            <allRegions/>
          </elemResult>
          <elemResult type="optResult_21">
            <allRegions/>
          </elemResult>
          <elemResult type="optResult_22">
            <allRegions/>
          </elemResult>
          <elemResult type="optResult_23">
            <allRegions/>
          </elemResult>
          <elemResult type="mechTensor">
            <allRegions/>
          </elemResult>
          <nodeResult type="mechDisplacement">
            <allRegions/>
          </nodeResult>
        </storeResults>
      </mechanic>
    </pdeList>
    
<!--     <linearSystems> -->
<!--       <system> -->
<!--         <solverList> -->
<!--           <cholmod/> -->
<!--         </solverList> -->
<!--       </system> -->
<!--     </linearSystems> -->
  </sequenceStep>
    
  <optimization>
    <costFunction type="compliance" task="minimize" sequence="1" linear="false">
      <stopping queue="55" value="0.0001" type="relativeCostChange" maxHours="48"/>
    </costFunction>

<!--     <constraint type="distance"  bound="upperBound" value="0.5" design="node" /> -->
<!--     <constraint type="bending"  bound="upperBound" value="0.5" design="spaghetti" /> -->
<!--     <constraint type="bending" design="spaghetti" bound="upperBound" value="2" linear="false" mode="observation"/> -->
<!--     <constraint type="volume" bound="upperBound" value="0.5" mode="constraint" linear="false"/> -->
   
    <optimizer type="snopt" maxIterations="5">
      <snopt>
        <option name="major_optimality_tolerance" type="real" value="1e-8"/>
        <option type="integer" name="verify_level" value="-1"/>
      </snopt>
    </optimizer>
    
    <ersatzMaterial region="mech" material="mechanic" method="spaghetti">
      <spaghetti combine="p-norm" boundary="poly" transition=".05" radius=".25">
        <python file="spaghetti.py" path="cfs:share:python" >
          <!-- silent disables command line output from pythons-->
          <option key="silent" value="1"/>
          <option key="order" value="3"/>
          <option key="p" value="8"/>
        </python> 
        <noodle segments="2">
          <node dof="x" initial="0." upper="1" lower="0" tip="start"/>
          <node dof="y" initial="0.0626311" upper="1" lower="0"  tip="start"/>
          <node dof="x" initial="1" upper="1" lower="0"  tip="end"/>
          <node dof="y" initial="0.0546454" upper="1" lower="0" tip="end"/>
          <profile fixed=".17"/>
          <normal initial="0." lower="-0.2" upper="0.2"/>
        </noodle>

        <noodle segments="2">
          <node dof="x" initial="0.0156926" upper="1" lower="0" tip="start"/>
          <node dof="y" initial="0.974114" upper="1" lower="0"  tip="start"/>
          <node dof="x" initial="1" upper="1" lower="0"  tip="end"/>
          <node dof="y" initial="0.077333" upper="1" lower="0" tip="end"/>
          <profile fixed=".17"/>
          <normal initial="0.0" lower="-0.2" upper="0.2"/>
        </noodle>
      </spaghetti>
    
      <design name="density" initial=".5" physical_lower="1e-3" upper="1.0"/>

      <transferFunction type="identity" application="mech" design="density" />
      
      <!--       <result value="costGradient" id="optResult_1" design="density" />  
       <result value="costGradient" id="optResult_2" design="rotAngle" /> 
       <result value="genericElem" generic="d_compliance_density_by_d_s0_px" id="optResult_3" design="density"/>
       <result value="genericElem" generic="d_compliance_density_by_d_s0_py" id="optResult_4" design="density"/>
       <result value="genericElem" generic="d_compliance_density_by_d_s0_qx" id="optResult_5" design="density"/>
       <result value="genericElem" generic="d_compliance_density_by_d_s0_qy" id="optResult_6" design="density"/>
       <result value="genericElem" generic="d_compliance_rotAngle_by_d_s0_px" id="optResult_7" design="rotAngle"/>
       <result value="genericElem" generic="d_compliance_rotAngle_by_d_s0_py" id="optResult_8" design="rotAngle"/>
       <result value="genericElem" generic="d_compliance_rotAngle_by_d_s0_qx" id="optResult_9" design="rotAngle"/>
       <result value="genericElem" generic="d_compliance_rotAngle_by_d_s0_qy" id="optResult_10" design="rotAngle"/>
       <result value="genericElem" generic="d_compliance_density_by_d_s0_a1" id="optResult_11" design="density"/> 
       <result value="genericElem" generic="d_compliance_rotAngle_by_d_s0_a1" id="optResult_12" design="rotAngle"/>
       <result value="genericElem" generic="d_compliance_density_by_d_s1_px" id="optResult_13" design="density"/>
       <result value="genericElem" generic="d_compliance_density_by_d_s1_py" id="optResult_14" design="density"/>
       <result value="genericElem" generic="d_compliance_density_by_d_s1_qx" id="optResult_15" design="density"/>
       <result value="genericElem" generic="d_compliance_density_by_d_s1_qy" id="optResult_16" design="density"/>
       <result value="genericElem" generic="d_compliance_rotAngle_by_d_s1_px" id="optResult_17" design="rotAngle"/>
       <result value="genericElem" generic="d_compliance_rotAngle_by_d_s1_py" id="optResult_18" design="rotAngle"/>
       <result value="genericElem" generic="d_compliance_rotAngle_by_d_s1_qx" id="optResult_19" design="rotAngle"/>
       <result value="genericElem" generic="d_compliance_rotAngle_by_d_s1_qy" id="optResult_20" design="rotAngle"/>
       <result value="genericElem" generic="d_compliance_density_by_d_s1_a1" id="optResult_21" design="density"/> 
       <result value="genericElem" generic="d_compliance_rotAngle_by_d_s1_a1" id="optResult_22" design="rotAngle"/> -->
      <export save="all" write="iteration"/>
      
    </ersatzMaterial>
    <commit mode="forward" stride="1"/>
  </optimization>
</cfsSimulation>
