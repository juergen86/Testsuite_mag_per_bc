<?xml version="1.0"?>

 <cfsSimulation xmlns="http://www.cfs++.org/simulation" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.cfs++.org/simulation http://cfs-doc.mdmt.tuwien.ac.at/xml/CFS-Simulation/CFS.xsd">
   <documentation>
     <title>Permantent magnet with coil exciting a plate</title>
     <authors>
       <author>kaltenbacher hauck</author>
     </authors>
     <date>2004-07-14</date>
     <keywords>
       <keyword>acoustic</keyword>
     </keywords>
     <references>n.a. </references>
     <isVerified>no</isVerified>
     <description>
       Proof of concept example for magneto-mechanical coupling for a 
       setup including a permanent magnet and a coil.
       
       In a first static analysis, only the permanent magnet is active.
       In the second step, an additional coil is present and the 
       static field from the first run is taken as initial value.
       Due to eddy currents in the bottom conductive material, 
       Lorentz forces are generated and the structure starts 
       to vibrate.
     </description>
   </documentation>
   
   <fileFormats>
     <output>
       <hdf5/>
     </output>
     <materialData file="mat.xml" format="xml"/>
   </fileFormats>
   
   <domain geometryType="plane">
     <regionList>
       <region name="air"  material="AIR"/>
       <region name="coil" material="AIR"/>
       <region name="perm" material="AIR"/>
       <region name="material" material="aluminium"/>
     </regionList>
     
     <nodeList>
       <nodes name="fix_az"/>
       <nodes name="fix_ux"/>
       <nodes name="fix_uxuy"/>
     </nodeList>
     
   </domain>
   
   <!-- First analysis step (static) -->
   <sequenceStep index="1">
     <analysis>
       <static/>
     </analysis>
     
     <pdeList>
       
       <magnetic>
         <regionList>
           <region name="air"/>
           <region name="coil"/>
           <region name="perm"/>
           <region name="material"/>
         </regionList>
         <bcsAndLoads>
           <fluxParallel name="fix_az">
             <comp dof="z"/>
           </fluxParallel>
           
           <fluxDensity name="perm">
             <comp dof="y" value="1"/>
           </fluxDensity>
         </bcsAndLoads>
         
         <storeResults>
           <nodeResult type="magPotential">
             <allRegions/>
           </nodeResult>
           <elemResult type="magFluxDensity">
             <allRegions/>          
           </elemResult>
           <elemResult type="magEddyCurrentDensity">
             <allRegions/>
           </elemResult>
         </storeResults>
         
       </magnetic>
     </pdeList>
   </sequenceStep>
   
   <!-- First analysis step (transient) -->
   <sequenceStep index="2">
     <analysis>
       <transient>
         <numSteps>5</numSteps>
         <deltaT>1e-04</deltaT>
       </transient>
     </analysis>
     
     <pdeList>
       
       <mechanic subType="planeStrain">
         <regionList>
           <region name="material"/>
         </regionList>
         <bcsAndLoads>
           <fix name="fix_ux">
             <comp dof="x"/>
           </fix>
           <fix name="fix_uxuy">
             <comp dof="x"/>
             <comp dof="y"/>
           </fix>
           <!-- ========================= -->
           <!--  Iterative Coupling       -->
           <!-- ========================= -->
           <forceDensity name="material">
             <coupling pdeName="magnetic">
               <quantity name="magForceLorentzDensity"/>
             </coupling>
           </forceDensity>
         </bcsAndLoads>
         <storeResults>
           <nodeResult type="mechDisplacement">
             <allRegions/>
           </nodeResult>
         </storeResults>
       </mechanic>
       
       
       <magnetic>
         <regionList>
           <region name="air"/>
           <region name="coil"/>
           <region name="perm"/>
           <region name="material"/>
         </regionList>

        <initialValues>
          <initialState>
            <sequenceStep index="1"/>
          </initialState>
        </initialValues>
         
         <bcsAndLoads>
           <fluxParallel name="fix_az">
             <comp dof="z"/>
           </fluxParallel>
           <fluxDensity name="perm">
             <comp dof="y" value="1"/>
           </fluxDensity>
         </bcsAndLoads>
         
         <coilList>
           <coil id="myCoil">
            <source type="current" value="200"/>
              <part>
                <regionList>
                  <region name="coil"/>
                </regionList>
		<direction>
		  <analytic/>
		</direction>
                <wireCrossSection area="5e-7"/>
                <resistance value="0"/> 
              </part>
            </coil>
          </coilList>
         
         <storeResults>
           <nodeResult type="magPotential">
             <allRegions/>
           </nodeResult>
           <elemResult type="magFluxDensity">
             <allRegions/>          
           </elemResult>
           <elemResult type="magEddyCurrentDensity">
             <allRegions/>
           </elemResult>
           <elemResult type="magForceLorentzDensity">
             <regionList>
               <region name="material"/>
             </regionList>
           </elemResult>
           <regionResult type="magForceLorentz">
             <regionList>
               <region name="material"/>
             </regionList>
           </regionResult>
         </storeResults>
       </magnetic>
       
       
     </pdeList>
     
     <couplingList>
       <iterative>
         <!-- Specify convergence criterias -->
         <convergence logging="yes" maxNumIters="20" stopOnDivergence="no">
           <quantity name="magForceLorentz" value="1e-3" normType="rel"/>
           <quantity name="mechDisplacement" value="1e-3" normType="rel"/>
         </convergence>
         <!-- Define geometric update -->
         <geometryUpdate>
           <region name="material"/>
         </geometryUpdate>
       </iterative>
     </couplingList>

    <linearSystems>
      <system>
        <solverList>
          <directLU/>
        </solverList>
      </system>
    </linearSystems>

   </sequenceStep>
 </cfsSimulation>
 