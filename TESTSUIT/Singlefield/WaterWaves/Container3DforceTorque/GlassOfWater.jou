reset

# variables
# cylindric container Radius R = 30 mm, height H = 30 mm
#{r = 0.03 } # radius (fluid)
#{h = 0.030 } # height (fluid)
#{tw = 0.002} # glass thickness (wall)
#{tb = 0.005} # glass thickness (bottom) 
#{rx = r+tw} # radius (x-direction of elliptical cross section) at upper edge
#{ry = rx}
#{R = rx} # radius at top edge
#{H = h+tb} # height

create frustum height {H} major radius {rx} minor radius {ry} top {R}
create frustum height {H} major radius {rx-tw} minor radius {ry-tw} top {R-tw}
move Volume 2 z {0}
#subtract body 2  from body 1
webcut volume 1  with sheet extended from surface 4 
delete volume 2
webcut volume all with plane zplane offset {-H/2+tb}

# imprint & merge for conforming mesh
imprint volume all 
merge volume all 

# mesh
#{he = 0.008} # element size (used vor height division only)
volume all size {he}
curve all interval 64 # Number of elements around the circumference
curve all  scheme equal
mesh curve all 
surface 9 13 scheme hole rad_intervals 1 # wall thickness division
surface 11 12 19 scheme circle # the horizontal surfaces

# select regions
block 1 add volume 3 
block 1 name "V_water"
block 2 add volume 1 4 5
block 2 name "V_glass"

nodeset 1 add surface 12  
nodeset 1 name "S_surface"
nodeset 2 add surface 14 19
nodeset 2 name "S_glass-water"
nodeset 4 add surface 8 11 
nodeset 4 name "S_glass-bottom"

webcut volume all with plane yplane offset 0 
delete volume with y_coord < 0
webcut volume all with plane xplane offset 0
imprint volume all 
merge volume all

#sweep surface 65 72  perpendicular distance { 0.105 - h } 

move Volume all z {-H/2} include_merged 

nodeset 5 add surface in volume in block 2 with y_coord=0
nodeset 5 name "S_sym-glass"

nodeset 6 add vertex 41
nodeset 6 name "P_0"

# somehow this does not select
select vertex with x_coord = 0 and y_coord = 0 and (z_coord=(-0.03))

nodeset 7 add vertex 22
nodeset 7 name "P_1"

# element type and mesh
block all element type hex20
mesh volume all

#smooth
surface all smooth scheme laplacian
smooth surface all

export ansys cdb "GlassOfWater.cdb"  geometry overwrite 
