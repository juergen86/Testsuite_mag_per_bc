<?xml version="1.0" encoding="UTF-8"?>
<cfsSimulation xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" 
               xsi:schemaLocation="http://www.cfs++.org/simulation ../../../../../share/xml/CFS-Simulation/CFS.xsd"
               xmlns="http://www.cfs++.org/simulation">

  <documentation>
    <title>RotatedChamberL2Norm2D</title>
    <authors>
      <author>dmayrhof, pheidegger</author>
    </authors>
    <date>2024-04-16</date>
    <keywords>
      <keyword>smooth</keyword>
      <keyword>output</keyword>
    </keywords>
    <references> 
      Non Just functionality test
    </references>
    <isVerified>yes</isVerified>
    <description>
      Transient excitation of a chamber that gets compressed to half its size. We evaluate the L2Norm postprocessing result which uses the integrated function instead of the nodal L2 norm. We compare four cases, where we test either correct or too low integration order as well as the computation via only the PDE-result or only the reference solution. Additionally, the domain is rotated 30° to check if the vectorial components are incorporated correctly.   
      This test case covers the functionality computeSolutionSteps="last" to compute the L2 norm only for the last time step
    </description>
  </documentation>
  
  <fileFormats>
    <input>
      <hdf5 fileName="RotatedChamberL2Norm2D.h5ref"/>      
    </input>
    <output>
      <hdf5 id="h5"/>
      <text id="txt"/>
    </output>
    <materialData file="mat.xml" format="xml"/>
  </fileFormats>

  <domain geometryType="plane">
  	<variableList>
      <var name="d" value="1e-1"/>
  	  <var name="f_exc" value="100"/>
  	  <var name="amp" value="-d/2"/>
      <var name="rot_angle" value="30" />
    </variableList>
  
    <regionList>
      <region name="Chamber" material="FluidMat"/>
    </regionList>
    
    <surfRegionList>
      <surfRegion name="C_Exc"/>
      <surfRegion name="C_Side"/>
      <surfRegion name="C_Fix"/>
    </surfRegionList>
  </domain>

  <sequenceStep index="1"> <!-- Correct integration order, 0 solution reference -->
    <analysis>
      <transient initialTime="zero">
        <numSteps>12</numSteps>
        <deltaT>1e-3</deltaT>
      </transient>
    </analysis>
    
    <pdeList>
      <smooth subType="planeStrain">
        <regionList>
          <region name="Chamber"/>
        </regionList>
        
        <bcsAndLoads>
          <displacement name="C_Exc">
            <comp dof="x" value="((t lt 1/f_exc)? amp*1/2*(1-cos(pi*f_exc*t))*(-sin(rot_angle*pi/180)) : (amp)*(-sin(rot_angle*pi/180)))"/>
            <comp dof="y" value="((t lt 1/f_exc)? amp*1/2*(1-cos(pi*f_exc*t))*cos(rot_angle*pi/180) : (amp)*cos(rot_angle*pi/180))"/>
          </displacement>
          <fix name="C_Fix">
            <comp dof="x"/>
            <comp dof="y"/>
          </fix>
        </bcsAndLoads>
        
        <storeResults>
          <nodeResult type="smoothDisplacement">
            <allRegions outputIds="h5" postProcId="L2"/>
          </nodeResult>
        </storeResults>
      </smooth>   
    </pdeList>
    
    <postProcList>
      <postProc id="L2">
        <L2Norm resultName="L2" outputIds="txt,h5" integrationOrder="2" mode="absolute">
          <dof name="x" realFunc="0" />
          <dof name="y" realFunc="0" />
        </L2Norm>
      </postProc>
    </postProcList>
  </sequenceStep>

  <sequenceStep index="2"> <!-- Correct integration order, real solution reference, no excitation -->
    <analysis>
      <transient initialTime="zero">
        <numSteps>12</numSteps>
        <deltaT>1e-3</deltaT>
      </transient>
    </analysis>
    
    <pdeList>
      <smooth subType="planeStrain">
        <regionList>
          <region name="Chamber"/>
        </regionList>
        
        <bcsAndLoads>
          <displacement name="C_Exc">
            <comp dof="x" value="0"/>
            <comp dof="y" value="0"/>
          </displacement>
          <fix name="C_Fix">
            <comp dof="x"/>
            <comp dof="y"/>
          </fix>
        </bcsAndLoads>
        
        <storeResults>
          <nodeResult type="smoothDisplacement">
            <allRegions outputIds="h5" postProcId="L2sol"/>
          </nodeResult>
        </storeResults>
      </smooth>   
    </pdeList>
    
    <postProcList>
      <postProc id="L2sol">
        <L2Norm resultName="L2sol" outputIds="txt,h5" integrationOrder="2" mode="absolute">
          <dof name="x" realFunc="((t lt 1/f_exc)? amp*(y*cos(rot_angle*pi/180)-x*sin(rot_angle*pi/180))/d*1/2*(1-cos(pi*f_exc*t))*(-sin(rot_angle*pi/180)) : (amp))*(y*cos(rot_angle*pi/180)-x*sin(rot_angle*pi/180))/d*(-sin(rot_angle*pi/180))"/>
          <dof name="y" realFunc="((t lt 1/f_exc)? amp*(y*cos(rot_angle*pi/180)-x*sin(rot_angle*pi/180))/d*1/2*(1-cos(pi*f_exc*t))*cos(rot_angle*pi/180) : (amp))*(y*cos(rot_angle*pi/180)-x*sin(rot_angle*pi/180))/d*cos(rot_angle*pi/180)"/>
        </L2Norm>
      </postProc>
    </postProcList>
  </sequenceStep>

  <sequenceStep index="3"> <!-- Integration order too low, 0 solution reference -->
    <analysis>
      <transient initialTime="zero">
        <numSteps>12</numSteps>
        <deltaT>1e-3</deltaT>
      </transient>
    </analysis>
    
    <pdeList>
      <smooth subType="planeStrain">
        <regionList>
          <region name="Chamber"/>
        </regionList>
        
        <bcsAndLoads>
          <displacement name="C_Exc">
            <comp dof="x" value="((t lt 1/f_exc)? amp*1/2*(1-cos(pi*f_exc*t))*(-sin(rot_angle*pi/180)) : (amp)*(-sin(rot_angle*pi/180)))"/>
            <comp dof="y" value="((t lt 1/f_exc)? amp*1/2*(1-cos(pi*f_exc*t))*cos(rot_angle*pi/180) : (amp)*cos(rot_angle*pi/180))"/>
          </displacement>
          <fix name="C_Fix">
            <comp dof="x"/>
            <comp dof="y"/>
          </fix>
        </bcsAndLoads>
        
        <storeResults>
          <nodeResult type="smoothDisplacement">
            <allRegions outputIds="h5" postProcId="L2low"/>
          </nodeResult>
        </storeResults>
      </smooth>   
    </pdeList>

    <postProcList>
      <postProc id="L2low">
        <L2Norm resultName="L2low" outputIds="txt,h5" integrationOrder="1" mode="absolute">
          <dof name="x" realFunc="0" />
          <dof name="y" realFunc="0" />
        </L2Norm>
      </postProc>
    </postProcList>
  </sequenceStep>

  <sequenceStep index="4"> <!-- Integration order too low, real solution reference, no excitation -->
    <analysis>
      <transient initialTime="zero">
        <numSteps>12</numSteps>
        <deltaT>1e-3</deltaT>
      </transient>
    </analysis>
    
    <pdeList>
      <smooth subType="planeStrain">
        <regionList>
          <region name="Chamber"/>
        </regionList>
        
        <bcsAndLoads>
          <displacement name="C_Exc">
            <comp dof="x" value="0"/>
            <comp dof="y" value="0"/>
          </displacement>
          <fix name="C_Fix">
            <comp dof="x"/>
            <comp dof="y"/>
          </fix>
        </bcsAndLoads>
        
        <storeResults>
          <nodeResult type="smoothDisplacement">
            <allRegions outputIds="h5" postProcId="L2solLow"/>
          </nodeResult>
        </storeResults>
      </smooth>   
    </pdeList>
    
    <postProcList>
      <postProc id="L2solLow">
        <L2Norm resultName="L2solLow" outputIds="txt,h5" integrationOrder="1" mode="absolute">
          <dof name="x" realFunc="((t lt 1/f_exc)? amp*(y*cos(rot_angle*pi/180)-x*sin(rot_angle*pi/180))/d*1/2*(1-cos(pi*f_exc*t))*(-sin(rot_angle*pi/180)) : (amp))*(y*cos(rot_angle*pi/180)-x*sin(rot_angle*pi/180))/d*(-sin(rot_angle*pi/180))"/>
          <dof name="y" realFunc="((t lt 1/f_exc)? amp*(y*cos(rot_angle*pi/180)-x*sin(rot_angle*pi/180))/d*1/2*(1-cos(pi*f_exc*t))*cos(rot_angle*pi/180) : (amp))*(y*cos(rot_angle*pi/180)-x*sin(rot_angle*pi/180))/d*cos(rot_angle*pi/180)"/>
        </L2Norm>
      </postProc>
    </postProcList>
  </sequenceStep>

  <sequenceStep index="5"> <!-- Relative L2Norm: Correct integration order, 0 solution reference -->
    <analysis>
      <transient initialTime="zero">
        <numSteps>12</numSteps>
        <deltaT>1e-3</deltaT>
      </transient>
    </analysis>
    
    <pdeList>
      <smooth subType="planeStrain">
        <regionList>
          <region name="Chamber"/>
        </regionList>
        
        <bcsAndLoads>
          <displacement name="C_Exc">
            <comp dof="x" value="((t lt 1/f_exc)? amp*1/2*(1-cos(pi*f_exc*t))*(-sin(rot_angle*pi/180)) : (amp)*(-sin(rot_angle*pi/180)))"/>
            <comp dof="y" value="((t lt 1/f_exc)? amp*1/2*(1-cos(pi*f_exc*t))*cos(rot_angle*pi/180) : (amp)*cos(rot_angle*pi/180))"/>
          </displacement>
          <fix name="C_Fix">
            <comp dof="x"/>
            <comp dof="y"/>
          </fix>
        </bcsAndLoads>
        
        <storeResults>
          <nodeResult type="smoothDisplacement">
            <allRegions outputIds="h5" postProcId="L2rel"/>
          </nodeResult>
        </storeResults>
      </smooth>   
    </pdeList>
    
    <postProcList>
      <postProc id="L2rel">
        <L2Norm resultName="L2rel" outputIds="txt,h5" integrationOrder="2" mode="relative">
          <dof name="x" realFunc="0" />
          <dof name="y" realFunc="0" />
        </L2Norm>
      </postProc>
    </postProcList>
  </sequenceStep>

  <sequenceStep index="6"> <!-- Relative L2Norm: Correct integration order, real solution reference, no excitation -->
    <analysis>
      <transient initialTime="zero">
        <numSteps>12</numSteps>
        <deltaT>1e-3</deltaT>
      </transient>
    </analysis>
    
    <pdeList>
      <smooth subType="planeStrain">
        <regionList>
          <region name="Chamber"/>
        </regionList>
        
        <bcsAndLoads>
          <displacement name="C_Exc">
            <comp dof="x" value="0"/>
            <comp dof="y" value="0"/>
          </displacement>
          <fix name="C_Fix">
            <comp dof="x"/>
            <comp dof="y"/>
          </fix>
        </bcsAndLoads>
        
        <storeResults>
          <nodeResult type="smoothDisplacement">
            <allRegions outputIds="h5" postProcId="L2relSol"/>
          </nodeResult>
        </storeResults>
      </smooth>   
    </pdeList>
    
    <postProcList>
      <postProc id="L2relSol">
        <L2Norm resultName="L2relSol" outputIds="txt,h5" integrationOrder="2" mode="relative">
          <dof name="x" realFunc="((t lt 1/f_exc)? amp*(y*cos(rot_angle*pi/180)-x*sin(rot_angle*pi/180))/d*1/2*(1-cos(pi*f_exc*t))*(-sin(rot_angle*pi/180)) : (amp))*(y*cos(rot_angle*pi/180)-x*sin(rot_angle*pi/180))/d*(-sin(rot_angle*pi/180))"/>
          <dof name="y" realFunc="((t lt 1/f_exc)? amp*(y*cos(rot_angle*pi/180)-x*sin(rot_angle*pi/180))/d*1/2*(1-cos(pi*f_exc*t))*cos(rot_angle*pi/180) : (amp))*(y*cos(rot_angle*pi/180)-x*sin(rot_angle*pi/180))/d*cos(rot_angle*pi/180)"/>
        </L2Norm>
      </postProc>
    </postProcList>
  </sequenceStep>

  <sequenceStep index="7"> <!-- Relative L2Norm: Integration order too low, 0 solution reference -->
    <analysis>
      <transient initialTime="zero">
        <numSteps>12</numSteps>
        <deltaT>1e-3</deltaT>
      </transient>
    </analysis>
    
    <pdeList>
      <smooth subType="planeStrain">
        <regionList>
          <region name="Chamber"/>
        </regionList>
        
        <bcsAndLoads>
          <displacement name="C_Exc">
            <comp dof="x" value="((t lt 1/f_exc)? amp*1/2*(1-cos(pi*f_exc*t))*(-sin(rot_angle*pi/180)) : (amp)*(-sin(rot_angle*pi/180)))"/>
            <comp dof="y" value="((t lt 1/f_exc)? amp*1/2*(1-cos(pi*f_exc*t))*cos(rot_angle*pi/180) : (amp)*cos(rot_angle*pi/180))"/>
          </displacement>
          <fix name="C_Fix">
            <comp dof="x"/>
            <comp dof="y"/>
          </fix>
        </bcsAndLoads>
        
        <storeResults>
          <nodeResult type="smoothDisplacement">
            <allRegions outputIds="h5" postProcId="L2relLow"/>
          </nodeResult>
        </storeResults>
      </smooth>   
    </pdeList>

    <postProcList>
      <postProc id="L2relLow">
        <L2Norm resultName="L2relLow" outputIds="txt,h5" integrationOrder="1" mode="relative">
          <dof name="x" realFunc="0" />
          <dof name="y" realFunc="0" />
        </L2Norm>
      </postProc>
    </postProcList>
  </sequenceStep>

  <sequenceStep index="8"> <!-- Relative L2Norm: Integration order too low, real solution reference, no excitation -->
    <analysis>
      <transient initialTime="zero">
        <numSteps>12</numSteps>
        <deltaT>1e-3</deltaT>
      </transient>
    </analysis>
    
    <pdeList>
      <smooth subType="planeStrain">
        <regionList>
          <region name="Chamber"/>
        </regionList>
        
        <bcsAndLoads>
          <displacement name="C_Exc">
            <comp dof="x" value="0"/>
            <comp dof="y" value="0"/>
          </displacement>
          <fix name="C_Fix">
            <comp dof="x"/>
            <comp dof="y"/>
          </fix>
        </bcsAndLoads>
        
        <storeResults>
          <nodeResult type="smoothDisplacement">
            <allRegions outputIds="h5" postProcId="L2relSolLow"/>
          </nodeResult>
        </storeResults>
      </smooth>   
    </pdeList>
    
    <postProcList>
      <postProc id="L2relSolLow">
        <L2Norm resultName="L2relSolLow" outputIds="txt,h5" integrationOrder="1" mode="relative">
          <dof name="x" realFunc="((t lt 1/f_exc)? amp*(y*cos(rot_angle*pi/180)-x*sin(rot_angle*pi/180))/d*1/2*(1-cos(pi*f_exc*t))*(-sin(rot_angle*pi/180)) : (amp))*(y*cos(rot_angle*pi/180)-x*sin(rot_angle*pi/180))/d*(-sin(rot_angle*pi/180))"/>
          <dof name="y" realFunc="((t lt 1/f_exc)? amp*(y*cos(rot_angle*pi/180)-x*sin(rot_angle*pi/180))/d*1/2*(1-cos(pi*f_exc*t))*cos(rot_angle*pi/180) : (amp))*(y*cos(rot_angle*pi/180)-x*sin(rot_angle*pi/180))/d*cos(rot_angle*pi/180)"/>
        </L2Norm>
      </postProc>
    </postProcList>
  </sequenceStep>

  <sequenceStep index="9"> <!-- Correct integration order, 0 solution reference, only last timestep -->
    <analysis>
      <transient initialTime="zero">
        <numSteps>12</numSteps>
        <deltaT>1e-3</deltaT>
      </transient>
    </analysis>
    
    <pdeList>
      <smooth subType="planeStrain">
        <regionList>
          <region name="Chamber"/>
        </regionList>
        
        <bcsAndLoads>
          <displacement name="C_Exc">
            <comp dof="x" value="((t lt 1/f_exc)? amp*1/2*(1-cos(pi*f_exc*t))*(-sin(rot_angle*pi/180)) : (amp)*(-sin(rot_angle*pi/180)))"/>
            <comp dof="y" value="((t lt 1/f_exc)? amp*1/2*(1-cos(pi*f_exc*t))*cos(rot_angle*pi/180) : (amp)*cos(rot_angle*pi/180))"/>
          </displacement>
          <fix name="C_Fix">
            <comp dof="x"/>
            <comp dof="y"/>
          </fix>
        </bcsAndLoads>
        
        <storeResults>
          <nodeResult type="smoothDisplacement">
            <allRegions outputIds="h5" postProcId="L2Last"/>
          </nodeResult>
        </storeResults>
      </smooth>   
    </pdeList>
    
    <postProcList>
      <postProc id="L2Last">
        <L2Norm resultName="L2Last" outputIds="txt,h5" integrationOrder="2" mode="absolute" computeSolutionSteps="last">
          <dof name="x" realFunc="0" />
          <dof name="y" realFunc="0" />
        </L2Norm>
      </postProc>
    </postProcList>
  </sequenceStep>

  <sequenceStep index="10"> <!-- Relative L2Norm: Correct integration order, real solution reference, no excitation, only last timestep -->
    <analysis>
      <transient initialTime="zero">
        <numSteps>12</numSteps>
        <deltaT>1e-3</deltaT>
      </transient>
    </analysis>
    
    <pdeList>
      <smooth subType="planeStrain">
        <regionList>
          <region name="Chamber"/>
        </regionList>
        
        <bcsAndLoads>
          <displacement name="C_Exc">
            <comp dof="x" value="0"/>
            <comp dof="y" value="0"/>
          </displacement>
          <fix name="C_Fix">
            <comp dof="x"/>
            <comp dof="y"/>
          </fix>
        </bcsAndLoads>
        
        <storeResults>
          <nodeResult type="smoothDisplacement">
            <allRegions outputIds="h5" postProcId="L2relSolLast"/>
          </nodeResult>
        </storeResults>
      </smooth>   
    </pdeList>
    
    <postProcList>
      <postProc id="L2relSolLast">
        <L2Norm resultName="L2relSolLast" outputIds="txt,h5" integrationOrder="2" mode="relative" computeSolutionSteps="last">
          <dof name="x" realFunc="((t lt 1/f_exc)? amp*(y*cos(rot_angle*pi/180)-x*sin(rot_angle*pi/180))/d*1/2*(1-cos(pi*f_exc*t))*(-sin(rot_angle*pi/180)) : (amp))*(y*cos(rot_angle*pi/180)-x*sin(rot_angle*pi/180))/d*(-sin(rot_angle*pi/180))"/>
          <dof name="y" realFunc="((t lt 1/f_exc)? amp*(y*cos(rot_angle*pi/180)-x*sin(rot_angle*pi/180))/d*1/2*(1-cos(pi*f_exc*t))*cos(rot_angle*pi/180) : (amp))*(y*cos(rot_angle*pi/180)-x*sin(rot_angle*pi/180))/d*cos(rot_angle*pi/180)"/>
        </L2Norm>
      </postProc>
    </postProcList>
  </sequenceStep>
</cfsSimulation>
