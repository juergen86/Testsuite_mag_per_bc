# MagEdge3DWirepairPeriodicSym

Testcase for MagEdge pde with periodic boundary conditions applied using nonconforming interfaces on opposite parallel planes.

The Testcase consists of a pair of parallel wires located on x Axis with antiparallel current in z direction and an air brick [x:-1:1, y:-1.5:1.5, z:-d:d] sourrounding this pair of wires at [+-0,5, 0, -d:d] symmetric to the center of the brick.

4 Variants of this test case are implemented on this geometry using 4 steps of magnetostatic simulation with magneticEdge pde. 

### Step 1 Variant - no boundary condition: 
No boundary conditions applied. The resulting H-field is the superposition of the field excited by the current in each of the 2 wires as expected. 

### Step 2 Variant - periodic boundary condition in planes x=-1 and x=1: 
Periodic boundary conditions applied at planes x=-1 and x=+1 and boundary condition fluxparallel applied to planes z=-d and z=+d.
With this periodic boundary condtions work are approximated as expected with z-componente of H = 0. 

### Step 3 Variant - periodic boundary condition in planes x=-1 and x=1: 
Periodic boundary conditions applied at planes x=-1 and x=+1, but different to step 2 here is no enforcing of boundary condition flux parallel in planes normal zo z axis. In this case z components in H field appear and the H field vectors deviate from step 2 with growing radial distance from the wires. 

### Step 4 Variant - fluxparallel boundary condition in planes x=-1 and x=1: 
Fluxparallel boundary conditions applied at planes x=-1 and x=+1 and boundary condition fluxparallel applied to planes z=-d and z=+d.
With this periodic boundary condtions work are approximated as expected with z-componente of H = 0. 


