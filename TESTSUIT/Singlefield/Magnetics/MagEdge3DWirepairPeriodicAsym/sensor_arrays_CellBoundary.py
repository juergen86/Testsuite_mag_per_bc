# from params import *;
# python snippet to write circle coordinates in appropriate format 
# to define CFS sensorArray output.

# from numpy import sin,cos,arange, array,pi,zeros_like,savetxt,sqrt;
import numpy as np

###########################################################
# choose x,y grid
xmin        = -1.0
xmax        =  1.0
xr_steps    =  2

ymin        = -1.5
ymax        =  1.5
yr_steps    =  16

# asym .. asymmetrical shif of cell for simulation
asym = 0.0

###########################################################



dx  = np.linspace ( xmin + asym, xmax + asym, xr_steps )
dy  = np.linspace ( ymin, ymax, yr_steps )

X, Y = np.meshgrid(dx, dy)

# Transfrom cylinderic coordinates to cartesian coordinates
# X = r_m * np.cos(phi_m)
# Y = r_m * np.sin(phi_m)

# 
X_v = np.reshape ( X, X.size )
Y_v = np.reshape ( Y, Y.size )

P = np.array([X_v, Y_v, np.zeros_like(X_v)]);
np.savetxt('sensor_arrays.csv',P.T,delimiter=',')

pass
